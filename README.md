# Tiny Typing

A tiny typing game for tiny people.

## Description

When you press a key on the keyboard it appears on the screen and the name of the character is spoken.

## Features

- Runs fast on original Raspberry Pi running TinyCore Linux
- Can run without a window manager or X (directly from console)
